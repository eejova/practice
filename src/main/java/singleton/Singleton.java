package singleton;

public class Singleton {

    private static Singleton obj;

    private Singleton() {}

    public static Singleton getInstance()
    {
        if (obj==null)
            obj = new Singleton();
        return obj;
    }

    public void showMessage(){
        System.out.println("Well done!");
    }

    public static void main(String[] args) {

        //Get the only object available
        Singleton object = Singleton.getInstance();

        //show the message
        object.showMessage();
    }
}