package singleton;

public class ThreadSafeSingleton {
    private volatile static ThreadSafeSingleton instance;
    private String message = " ";
    private ThreadSafeSingleton(){
        System.out.println("Singleton instance created.");
    }
    public static ThreadSafeSingleton getInstance(){
        if (instance == null){
            synchronized (System.class){
                if (instance == null){
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public static void main(String[] args) {
        ThreadSafeSingleton instance = ThreadSafeSingleton.getInstance();
        instance.setMessage("Well done!Thread-safe singleton...");
        displayMessage();
    }

    public static void displayMessage(){
        ThreadSafeSingleton instance = ThreadSafeSingleton.getInstance();
        System.out.println(instance.getMessage());
    }
}