package ConsoleApp;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException, FileNotFoundException {


        FileInputStream in = null;
        FileOutputStream out = null;

        try{
            in = new FileInputStream("C:\\Users\\Katea\\IdeaProjects\\practice1\\src\\main\\java\\ConsoleApp\\input.txt");
            out = new FileOutputStream("C:\\Users\\Katea\\IdeaProjects\\practice1\\src\\main\\java\\ConsoleApp\\output.txt");

            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Katea\\IdeaProjects\\practice1\\src\\main\\java\\ConsoleApp\\input.txt"));
            String line = null;
            int even = 0;
            int odd = 0;
            int sumEven = 0;
            int sumOdd = 0;
            int totalNumbers = 0;
            int totalSum = 0;

            while ((line = br.readLine()) != null) {
                String[] values = line.split("[aA-zZ, .!?&;\".*\"'/-]+");

                for (String str : values) {
                    int number = Integer.parseInt(str);
                    if (number % 2 == 0){
                        even ++;
                        sumEven = sumEven + number;
                    }
                    else {
                        odd++;
                        sumOdd = sumOdd + number;
                    }

                    totalNumbers = even + odd;
                    totalSum = sumEven + sumOdd;
                    System.out.println(str);
                }
            }

            String content = "\nAmount of even numbers: " + even + "\nSum of even numbers: " + sumEven +
                    "\nAmount of odd numbers: " + odd + "\nSum of odd numbers: " + sumOdd +
                    "\nTotal amount of all numbers: " + totalNumbers +
                    "\nTotal sum of all numbers: " + totalSum;
            byte[] bytes = content.getBytes();
            out.write(bytes);
        }
        catch (IOException e) {
            System.out.print("Exception");
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}