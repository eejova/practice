package DoublyLinkedListImpl;

import java.util.Scanner;

public class Test {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        DoublyLinkedListImp linkedListImp = new DoublyLinkedListImp();

        System.out.println("Enter 7 integer numbers: ");
        for (int i = 0; i < 7; i ++){
            int data = scanner.nextInt();
            linkedListImp.addNodeAtTheEndOfTheList(data);
            linkedListImp.showDoublyLinkedList(linkedListImp.head);
        }

        System.out.println("Add a node at the beginning of a doubly linked list: ");
        linkedListImp.addNodeAtTheBeginning(89);
        linkedListImp.showDoublyLinkedList(linkedListImp.head);

        System.out.println("Add a node after given node: ");
        linkedListImp.addNodeAfterGivenNode(100, linkedListImp.head.next);
        linkedListImp.showDoublyLinkedList(linkedListImp.head);

        System.out.println("Search node: ");
        int searchNode = scanner.nextInt();
        linkedListImp.searchNode(searchNode);

        System.out.println("Length of linked list is: " + linkedListImp.linkedListLength());
    }
}