package DoublyLinkedListImpl;

public class DoublyLinkedListImp {
    Node head;

    public void addNodeAtTheBeginning(int newData){
        Node newNode = new Node(newData);
        newNode.next = head;
        newNode.previous = null;

        if (head != null){
            head.previous = newNode;
            head = newNode;
        }
    }

    public void addNodeAfterGivenNode(int newData, Node previousNode){
        if (previousNode == null){
            System.out.println("The given previous node cannot bu null");
            return;
        }
        Node newNode = new Node(newData);
        newNode.next = previousNode.next;
        previousNode.next = newNode;
        newNode.previous = previousNode;
        if (newNode.next != null){
            newNode.next.previous = newNode;
        }
        System.out.println();
    }

    void addNodeAtTheEndOfTheList(int newData){
        Node newNode = new Node(newData);
        Node last = head;
        if (head == null){
            newNode.previous = null;
            head = newNode;
            return;
        }
        while (last.next != null){
            last = last.next;
        }
        last.next = newNode;
        newNode.previous = last;
    }

    public void showDoublyLinkedList(Node node){
        System.out.println("Current Doubly Linked List:");
        while (node != null) {
            System.out.print(node.data + "<---->");
            node = node.next;
        }
        System.out.println();
    }

    public int linkedListLength() {
        int length = 0;
        Node current = head;
        while(current != null){
            length ++;
            current = current.getNext();
        }
        return length;
    }

    public void searchNode(int value) {
        int i = 1;
        boolean flag = false;
        Node current = head;

        if(head == null) {
            System.out.println("List is empty");
            return;
        }
        while(current != null) {
            if(current.data == value) {
                flag = true;
                break;
            }
            current = current.next;
            i++;
        }
        if(flag)
            System.out.println("Node " + value + " is present in the list at the position : " + i);
        else
            System.out.println(value + " is not present in the list");
    }
}