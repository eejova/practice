import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class LinkedListTest {

    public static void main(String args[]) {
        // Creating object of class linked list
        LinkedList<Integer> object = new LinkedList<>();

        // Adding elements to the linked list
        object.add(1);
        object.add(2);
        object.addLast(3);
        object.addFirst(9);
        object.add(2, 0);

        if(object == null){
            throw new NullPointerException();
        }
        System.out.println("Linked list : " + object);

        // Removing elements from the linked list
        object.remove(1);
        object.removeLast();
        System.out.println("Linked list after deletion: " + object);

        // Finding elements in the linked list
        boolean status = object.contains(9);

        if (status)
            System.out.println("List contains the element '9' ");
        else
            System.out.println("List doesn't contain the element '9'");

        // Number of elements in the linked list
        int size = object.size();
        System.out.println("Size of linked list = " + size);

        // Get and set elements from linked list
        Object element = object.get(2);
        System.out.println("Element returned by get() : " + element);
        object.set(2, 8);
        System.out.println("Linked list after change : " + object);

        // A collection is created
        Collection<Integer> collect = new ArrayList<Integer>();

        collect.add(100);
        collect.add(110);
        collect.add(120);
        collect.add(130);

        // Appending the collection to the list
        object.addAll(collect);

        // Clearing the list using clear() and displaying
        System.out.println("The new linked list is: " + object);

        // using get() to print element at first index
        System.out.println("Element at 1st index is : " + object.getFirst());

        System.out.println("The last element is: " + object.getLast());

        int newSize = object.size();
        System.out.println("Size of new linked list = " + newSize);

        // The last position of an element is returned
        System.out.println("Last occurrence of 130 is at index: "
                + object.lastIndexOf(130));
    }
}