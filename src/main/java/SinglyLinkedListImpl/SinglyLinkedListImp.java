package SinglyLinkedListImpl;

public class SinglyLinkedListImp {
    Node head;

    public void addNodeAtTheBeginning(int data){
        Node node = new Node(data);                     //create nod
        node.setNext(head);                             //setting the next reference of node to refer to head or start of list
        head = node;                                    //setting the head to refer node
    }

    public void addNodeAtTheEnd(int data){
        Node node = new Node(data);
        if (head == null){                              // checking if list is empty, if the list is empty we will insert node at head
            head = node;
        }
        else{                                           //assigning head to a temp node, to traverse the list and insert node at the end
            Node temp = head;
            while (temp.getNext() != null){             //a loop to reach end of linked list
                temp = temp.getNext();                  //if the next node is available, updating temp to refer next node
            }
            temp.setNext(node);                         //now temp is referring to last node of the list
        }
    }

    public int deleteNodeAtTheEnd() {
        int response = 0;
        if (head != null) {                             //checking if list contain any node
            Node temp = head;                           //to delete the last node, we have to reach the second last node, and set its next node reference to null
            Node previous = null;                       // assigning head to temp
                                                        //to store reference of previous node, we will use this node to delete last node by setting its next node as null
            while (temp.getNext() != null) {
                previous = temp;
                // updating temp
                temp = temp.getNext();                  //updating temp
            }

            response = temp.getData();                  //getting data from last node
                                                        //now previous is pointing to second last node
            if (previous != null) {                     //if there only one node in the list, then  update head
                previous.setNext(null);
            } else {
                head = null;
                System.out.println("List is empty");
            }
        }
        return response;
    }

    public int deleteNodeAtTheBeginning() {
        int response = 0;
        if (head != null) {
            response = head.getData();                  //getting data from first node
            head = head.getNext();                      // updating head to refer to next node
        }
        else{
            System.out.println("List is empty");
        }
        return response;
    }

    public void showLinkedList(){

        System.out.println("Current Singly Linked List:");
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.getData());
            temp = temp.getNext();
            if (temp != null) {
                System.out.print("---->");
            }
        }
        System.out.println();
    }

    public int linkedListLength() {
        int length = 0;
        Node current = head;
        while(current != null){
            length ++;
            current = current.getNext();
        }
        return length;
    }

    public boolean searchNode(Node head, int enteredValue) {

        Node current = head;                                    // assigning head to temp,will check each node for given data
        while (current != null)
        {
            if (current.data == enteredValue)                   // checking for data at each node
                return true;                                    // updating temp
            current = current.next;
        }
        return false;
    }
}