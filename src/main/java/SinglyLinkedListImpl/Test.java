package SinglyLinkedListImpl;

import java.util.Scanner;

public class Test {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        SinglyLinkedListImp linkedListImp = new SinglyLinkedListImp();

        System.out.println("Enter 7 integer numbers: ");
        for (int i = 0; i < 7; i ++){
            int data = scanner.nextInt();
            linkedListImp.addNodeAtTheEnd(data);
            linkedListImp.showLinkedList();
        }

        System.out.println("Delete last node ");
        int deletedNodeAtTheEnd = linkedListImp.deleteNodeAtTheEnd();
        System.out.println(deletedNodeAtTheEnd);
        linkedListImp.showLinkedList();

        System.out.println("Delete first node ");
        int deletedNodeAtTheBeginning = linkedListImp.deleteNodeAtTheBeginning();
        System.out.println(deletedNodeAtTheBeginning);
        linkedListImp.showLinkedList();

        System.out.println("Add node at the beginning: ");
        int data = scanner.nextInt();
        linkedListImp.addNodeAtTheBeginning(data);
        linkedListImp.showLinkedList();

        System.out.println("Search node: ");
        int searchNode = scanner.nextInt();
        if (linkedListImp.searchNode(linkedListImp.head, searchNode)){
            System.out.println("Node found: " + searchNode);
        }
        else {
            System.out.println("This element does not exists in linked list");
        }

        System.out.println("Length of linked list is: " + linkedListImp.linkedListLength());
    }
}