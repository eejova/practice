package Patterns.Operations;

public interface Factory {

    Object next();
}