package Patterns.Operations;

public class Instant extends MathOperation {

    public Instant(){
        description = "instant value";
    }

    public int getValue(){
        return 276;
    }
}