package Patterns.Operations;

public abstract class MathOperation{

    String description = "Some math operation";

    public String getDesc(){
        return description;
    }

    public abstract int getValue();
//    public int getValue(){
//        return 276;
//    }
}