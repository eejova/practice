package Patterns.Decorator;

import Patterns.Operations.MathOperation;

public class DivisionDecorator extends AddOnDecorator {

    MathOperation mathOperation;

    public DivisionDecorator(MathOperation mathOperation){
        this.mathOperation = mathOperation;
    }

    public String getDesc(){

        return mathOperation.getDesc() + " division";
    }

    public int getValue(){
        return this.getValue() / 3;
    }
}