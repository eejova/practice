package Patterns.Decorator;

import Patterns.Operations.MathOperation;

public class MultiplicationDecorator extends AddOnDecorator {

    MathOperation mathOperation;

    public MultiplicationDecorator(MathOperation mathOperation){
        this.mathOperation = mathOperation;
    }

    public String getDesc(){

        return mathOperation.getDesc() + " multiplication";
    }

    public int getValue(){
        return this.getValue() * 2;
    }
}