package Patterns.Decorator;

import Patterns.Operations.MathOperation;

public class AdditionDecorator extends AddOnDecorator {

    MathOperation mathOperation;

    public AdditionDecorator(MathOperation mathOperation){

        this.mathOperation = mathOperation;
    }

    public String getDesc(){

        return mathOperation.getDesc() + " addition";
    }

    public int getValue(){

        return this.getValue() + 1;
    }
}