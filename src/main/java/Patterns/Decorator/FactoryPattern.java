package Patterns.Decorator;

import Patterns.Operations.Instant;
import Patterns.Operations.MathOperation;

import java.util.ArrayList;

public class FactoryPattern{

    public static ArrayList<MathOperation> SomeOperation = new ArrayList<>();

    public static void main(String[] args) {

        MathOperation mathOperation = new Instant();
        mathOperation = new AdditionDecorator(mathOperation);
        mathOperation = new DivisionDecorator(mathOperation);
        mathOperation = new SubstractionDecorator(mathOperation);
        mathOperation = new MultiplicationDecorator(mathOperation);
        System.out.println(mathOperation.getDesc() + " = " + mathOperation.getValue());

        new NullObject();
    }
}