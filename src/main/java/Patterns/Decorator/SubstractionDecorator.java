package Patterns.Decorator;

import Patterns.Operations.MathOperation;

public class SubstractionDecorator extends AddOnDecorator {

    MathOperation mathOperation;

    public SubstractionDecorator(MathOperation mathOperation){
        this.mathOperation = mathOperation;
    }

    public String getDesc(){
        return mathOperation.getDesc() + " substraction";
    }

    public int getValue(){

        return this.getValue() - 4;
    }
}