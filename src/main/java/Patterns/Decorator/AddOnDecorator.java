package Patterns.Decorator;

import Patterns.Operations.MathOperation;

public abstract class AddOnDecorator extends MathOperation {

    public abstract String getDesc();
}